# Anticensorship Telegram Bot @IWantSeeThisPicksBot

# Description
> Telegram bot that allows you to view prohibited content on IOS devices. 
> You can add them to the group or just send them a post, and then they will resend all the images with the caption in 
> a viewable form.

# Usage
> Send blocked picture or animation to bot. You can use this bot in telegram now:  
>
https://t.me/IWantSeeThisPicksBot


---
# Run Local:

##### To install webhook automatically replace
``` {.python}
updater.start_webhook(listen="0.0.0.0", port=PORT, url_path=TELEGRAM_TOKEN)
updater.bot.setWebhook(f'https://{HEROKU_APP_NAME}.herokuapp.com/{TELEGRAM_TOKEN}')
```
to
``` {.python}
updater.start_polling()
```

##### Create and activate Python virtual environment.
`python3 -m venv venv`  
`source venv/bin/activate`

##### Install requirements.
`pip install -r requirements.txt`

##### Run Script.
`python main.py`

# Deploy to Heroku:

#### Install heroku-cli. 
```
Arch: yay -S heroku-cli
Ubuntu: sudo snap install heroku --classic
```

#### Login to Heroku. 
`heroku login`

#### Create Heroku project. 
`heroku create --region eu anticensorship-telegram-bot`

#### Connect Heroku to repository.
`heroku git:remote -a anticensorship-telegram-bot`

#### Set Heroku environment variables.
`heroku config:set HEROKU_APP_NAME=anticensorship-telegram-bot`  
`heroku config:set TELEGRAM_TOKEN=***TELEGRAM_BOT_TOKEN***`

#### Deploy to Heroku.
`git push heroku master`
