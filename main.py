import os
import logging

from telegram.ext import Updater, MessageHandler, Filters


# Logging.
logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# Required variables.
PORT = int(os.environ.get('PORT', 5000))
TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
HEROKU_APP_NAME = os.environ.get('HEROKU_APP_NAME')


# Context processor for photo.
def anticensorship_photo_handler(update, context):
    try:
        context.bot.send_photo(chat_id=update.effective_chat.id, photo=update.message.photo[-1]['file_id'], caption=update.message.caption)
    except Exception as e:
        logger.exception(e)
        context.bot.send_message(chat_id=update.effective_chat.id, text='Sended some bullshit or not photo.')


# Context processor for animation.
def anticensorship_animation_handler(update, context):
    try:
        context.bot.send_animation(chat_id=update.effective_chat.id, animation=update.message.animation['file_id'], caption=update.message.caption)
    except Exception as e:
        logger.exception(e)
        context.bot.send_message(chat_id=update.effective_chat.id, text='Sended some bullshit or not animation.')

# TODO: Add context processor for video.


# Creating main bot.
updater = Updater(token=TELEGRAM_TOKEN, use_context=True)
updater.dispatcher.add_handler(MessageHandler(Filters.photo, anticensorship_photo_handler))
updater.dispatcher.add_handler(MessageHandler(Filters.animation, anticensorship_animation_handler))

# Setting bot and webhook, for local server change to ```updater.start_polling()```
updater.start_webhook(listen="0.0.0.0", port=PORT, url_path=TELEGRAM_TOKEN)
updater.bot.setWebhook(f'https://{HEROKU_APP_NAME}.herokuapp.com/{TELEGRAM_TOKEN}')
